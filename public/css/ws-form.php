<style>
    form{
        display: block;
        margin-top: 0em;
    }

    form .wc-form-field{
        margin-bottom: 10px;
    }
    form .wc-form-control{
        border-radius: 0px;
        height: 40px;
        padding-left: 18px;
        border-color: #EEEEEE;
        background: transparent;
        display: block;
        width: 100%;
        font-size: 1rem;
        line-height: 1.5;
        overflow: visible;
    }
    footer .wc-form-control{
        background: white;
    }
    form input[type='checkbox']{
        display: block!important;
        width: 30px;
    }
    .wc-form-submit-btn{
        display: inline-block;
        border: 1px solid <?= $color_array[1];?>;
        text-transform: uppercase;
        font-size: 13px;
        font-weight: 500;
        padding: 10px 24px;
        background-color: <?= $color_array[1];?>;
        color: white;
        transition: all .4s ease;
    }
    @media (min-width: 900px){
        .button {
            padding: 12px 42px;
        }
    }
    .wc-form label{
        display: none;
    }
    .wc-form textarea{
        min-height: 150px;
    }

    .article{
        box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);
        border-radius: 2px;
        transition: all 0.5s ease;
        overflow: hidden;
    }
    .article:hover{
        margin-top: -5px;
        box-shadow: 0 20px 30px 0 rgba(0, 0, 0, 0.2);
    }
    .text-box{
        padding: 15px 10px 10px 10px;
    }
    .text-box > h2{
        color: white;
        text-shadow: 1px 2px 4px rgba(0, 0, 0, 0.8);
    }
    form{
        margin-top: 30px;
    }

</style>