<?php

class Bootstrap
{

    /** @var null The api set in the first time */
    private $api_key = null;

    /** @var null The controller */
    private $url_controller = null;

    /** @var null The method (of the above controller), often also named "action" */
    private $url_action = null;

    /** @var null Parameter one */
    private $url_parameter_1 = null;

    /** @var null Parameter two */
    private $url_parameter_2 = null;

    /** @var null Parameter three */
    private $url_parameter_3 = null;

    private $page_name = null;

    private $article_name = null;

    private function api_curl($key, $version = 'v2')
    {

        $url = 'https://winicms.com/api/' . $key . '/' . $version;
        //$url = __DIR__."/../../public/$key.json";
        return @file_get_contents($url);
        $ssl = true;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            $ssl = false;
        }

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => $ssl,
            CURLOPT_VERBOSE => 1,
        ]);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * "Start" the application:
     * Analyze the URL elements and calls the according controller/method or the fallback
     */
    public function __construct()
    {
        // create array with URL parts in $url
        $this->splitUrl();

        // no api key defined
        if ($this->api_key == null) {
            // we can call the domain deployed here to compare blabla
            require './application/controller/errorController.php';
            $home = new ErrorController();
            $home->index();
        } else {
            if (strlen($this->api_key) >= 43) {
                $response = json_decode($this->api_curl($this->api_key), true);

                if ($response != null && isset($response['success']) && $response['success'] === true) {
                    // start redirecting to the require page
                    require './application/controller/PageController.php';
                    $controller = new PageController();
                    $all_data = $response['data'];
                    $menu_array = $response['data']['menus']['main_menu'];
                    $api_key = $response['data']['api_key'];
                    $data_page = $response['data']['company_logo'];
                    $data_footer = $response['data']['footer'];
                    $couleur_primaire = $response['data']['color_primary'];
                    $couleur_secondaire = $response['data']['color_secondary'];
                    $couleur_tertiaire = $response['data']['color_tertiary'];
                    $color_array = [$couleur_primaire, $couleur_secondaire, $couleur_tertiaire];
                    if (isset($this->url_parameter_3)) {

                    } elseif (isset($this->url_parameter_2)) {
                        # code...
                    } elseif (isset($this->url_parameter_1)) {
                        # code...
                    } elseif (isset($this->url_action)) {
//                        foreach ($response['data']['menus']['main_menu'] as $menu){
                        //
                        //                            $menu_array['title'] = $menu['title'];
                        //                            $menu_array['name'] = $menu['name'];
                        //                            $menu_array['type'] = $menu['title'];
                        //                            array_push($menu_array, , $name, $type, $id, $link );
                        //
                        //                        }
                        // here we search for the page and then found the corresponding article
                        $article_found = false;
                        foreach ($response['data']['pages'] as $p) {
                            if ($p['name'] === $this->url_controller) {
                                // look for the article
                                foreach ($p['sections'] as $section) {
                                    foreach ($section['blocks'] as $block) {
                                        foreach ($block['data'] as $data) {
                                            if ($data['id'] === (int) $this->url_action) {
                                                // set param to redirect on this page
                                                $article = $data;
                                                $article_found = true;
                                                $controller->article($p, $article, $menu_array, $api_key, $data_page, $data_footer, $color_array, $all_data); // article of the controller page
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        // PAGE NOT FOUND
                        if ($article_found === false) {
                            require './application/controller/errorController.php';
                            $home = new ErrorController();
                            $home->index();
                        }
                        // article and page info
                    } else {
                        // $this->url_controller
                        $this->page = null;
                        if (empty($this->url_controller)) {
                            foreach ($response['data']['pages'] as $p) {
                                if ($p['name'] === 'accueil') {
                                    $controller->index($p, $menu_array, $api_key, $data_page, $data_footer, $color_array, $all_data); // passe all the content of the page
                                    break;
                                }
                            }
                        } else {
                            $page_found = false;
                            foreach ($response['data']['pages'] as $p) {
                                if ($p['name'] === $this->url_controller) {
                                    $controller->index($p, $menu_array, $api_key, $data_page, $data_footer, $color_array, $all_data);
                                    $page_found = true;
                                    break;
                                }
                            }

                            // PAGE NOT FOUND
                            if ($page_found === false) {
                                require './application/controller/errorController.php';
                                $home = new ErrorController();
                                $home->index();
                            }
                        }

                    }

                } else {
                    require './application/controller/errorController.php';
                    $home = new ErrorController();
                    $home->index();
                }
            } else {
                require './application/controller/errorController.php';
                $home = new ErrorController();
                $home->index();
            }
        }
    }
    /**
     * Get and split the URL
     */
    private function splitUrl()
    {
        if (isset($_GET)) {
            // split URL
            $url = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            // By the way, the syntax here is just a short form of if/else, called "Ternary Operators"
            // @see http://davidwalsh.name/php-shorthand-if-else-ternary-operators
            $this->api_key = ($_GET['api_key'] ?? null);
            $this->url_controller = ($_GET['page_name'] ?? null);
            $this->url_action = ($_GET['article'] ?? null);
            // $this->url_parameter_1 = (isset($url[3]) ? $url[3] : null);
            // $this->url_parameter_2 = (isset($url[4]) ? $url[4] : null);
            // $this->url_parameter_3 = (isset($url[5]) ? $url[5] : null);

            // for debugging. uncomment this if you have problems with the URL
            // echo 'Controller: ' . $this->url_controller . '<br />';
            // echo 'Action: ' . $this->url_action . '<br />';
            // echo 'Parameter 1: ' . $this->url_parameter_1 . '<br />';
            // echo 'Parameter 2: ' . $this->url_parameter_2 . '<br />';
            // echo 'Parameter 3: ' . $this->url_parameter_3 . '<br />';
        }
    }

    public function getApiKey(): ?string
    {
        return $this->api_key;
    }

    /**
     *@return
     */
    public function pageName(): ?string
    {
        return $this->page_name;
    }
}
