# WINICMS THEME SAMPLE

The good way to create a theme or website for **WINICMS Platform**, is using the *the winicms sample project* to enhance your work flow.

# Download
- The repository can be download directly or clone on your local machine

# Get Started
One you have downloaded *the sample code* on your local machine.
- Execute your project on your machine, according to your system 
```
http://localhost/<project_dir>/?api_key=<secret_key>
```

- Connect to [WINICMS](https://www.winicms.com/login)  assume you have already created a developer account or [Create a developer account now](https://winicms.com/developer/registration)
- Create a theme and then go to on the link below and replace **secret_key**  with your site secret key at the top right of your theme details page.

```
https://www.winicms.com/api/<secret_key>/v2
```
- Start adding content to your theme by click on button **Administrer le theme** below the secret_key on the top right corner of theme the details page

- Component can be customize according to your theme appearance, just by editing CSS and JS in the project structure

