<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class PageController extends Controller
{

    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($page,$menu_array,$api_key,$data_page,$data_footer,$color_array,$all_data)
    {
        // debug message to show where you are, just for the demo
//        echo 'Message from Controller: You are in the controller home, using the method index()';
        // load views. within the views we can echo out $songs and $amount_of_songs easily
//        var_dump($page);
        $all_data = $all_data;
        $color_array = $color_array;
        $color_p = $color_array[0];
        $color_s = $color_array[1];
        $color_t = $color_array[2];
        $logo = $data_page;
        $menu = $menu_array;
        $pages = $page;
        $data_footer = $data_footer;
        $api_key = $api_key;
        // load home page content
//        require 'application/views/_templates/_header.php';
        require 'application/views/_templates/layout.php';
//        require 'application/views/_templates/_footer.php';

    }

    public function article($page, $article, $menu_array,$api_key,$data_page,$data_footer,$color_array,$all_data)
    {
        $all_data = $all_data;
        $color_array = $color_array;
        $color_p = $color_array[0];
        $color_s = $color_array[1];
        $color_t = $color_array[2];
        $logo = $data_page;
        $menu = $menu_array;
        $pages = $page;
        $data_footer = $data_footer;
        $api_key = $api_key;
        $article = $article;
        // load home page content
        require 'application/views/_templates/details.php';
    }
}
