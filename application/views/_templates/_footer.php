<footer class="footer-area section-padding">
    <div class="container">
        <div class="row <?=  $data_footer['settings']['classes'] ?? '' ?>" style="display: grid;  grid-template-columns: <?= $data_footer['settings']['grid_template_columns']?> ;">
            <?php foreach ($data_footer['blocks'] as $datas): ?>
            <div style="padding: 4px;">
                <?php foreach ($datas['data'] as $past_date): ?>
                    <?php if ($past_date['type'] === 'custom'):?>
                        <?= $past_date['content']['html'];?>
                        <style>
                            <?= $past_date['content']['css'];?>
                        </style>
                        <script>
                            <?= $past_date['content']['js'];?>
                        </script>
                    <?php endif; ?>
                <?php if ($past_date['type'] === 'article'): ?>
                    <h2><?= $past_date['title']; ?></h2>
                    <p><?=  substr( $past_date['description'], 0, $past_date['setting']['character']); ?></p>
                        <?php if (strlen($past_date['description']) > $past_date['setting']['character']):?>
                            <a href="" class="wc-form-submit-btn"><?= $past_date['setting']['btn']['label']; ?></a>
                        <?php endif; ?>
                <?php endif; ?>
                 <?php if ($past_date['type'] === 'form'): ?>
                   <h3> <?= $past_date['title']; ?></h3>
                    <?= $past_date['content']; ?>
                <?php endif; ?>
                <?php endforeach;?>
            </div>
            <?php endforeach;?>
        </div>

        <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
        <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tous droits reservés | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://winicms.com" target="_blank">WiniCMS</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
      </div>
    </div>
  </footer>