<?php

/**
 *
 * @package php-mvc
 * @author Ambroise BAZIE
 * @license http://opensource.org/licenses/MIT MIT License
 */

// var_dump($_GET);
// die();
// load the (optional) Composer auto-loader
if (file_exists('vendor/autoload.php')) {
    require 'vendor/autoload.php';
}

// load application config (error reporting etc.)
require 'application/config/config.php';

// load application class
require 'application/libs/bootstrap.php';
require 'application/libs/controller.php';

// start the application
$app = new Bootstrap();
