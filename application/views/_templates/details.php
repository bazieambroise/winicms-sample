<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> <?= $all_data['company_name'] ?? 'WINI-SITE';?></title>
    <link rel="icon" href="https://winicms.com/uploads/<?=$all_data['company_logo'];?>" type="image/png">

    <link rel="stylesheet" href="../../../public/vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../../public/vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../../public/vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="../../../public/vendors/linericon/style.css">
    <link rel="stylesheet" href="../../../public/vendors/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../../public/vendors/owl-carousel/owl.carousel.min.css">
<!--    <link rel="stylesheet" href="../../../public/css/wini.css">-->


    <?php
    require 'public/css/style.php';
    require 'public/css/slide.php';
    require 'public/css/ws-form.php';
    require 'public/css/wini_css.php';
    ?>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../../public/css/viewer.css">
</head>
<body>
<div id="fb-root"></div>
<!--<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2"></script>-->
<?php
include 'application/views/_templates/_header.php';
$upload = "https://winicms.com/uploads/";
?>
<div class="container mt-30" style="margin-top: 40px;">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/<?= $api_key;?>">Accueil</a></li>
            <?php if ($page['name'] != "accueil"): ?>
            <li class="breadcrumb-item"><a href="/<?= $api_key.'/'.$men['name'];?>"><?= $page['name']?></a></li>
            <?php endif; ?>
            <li class="breadcrumb-item active" aria-current="page"><?= $article['title']; ?></li>
        </ol>
    </nav>
</div>
<section class="blog-post-area section-margin">
        <div class="container">
            <div class="wini_grid1 wini_two-block_2-1_">
                <div >
                    <div class="main_blog_details">
                        <?php if (isset($article['medias'])):?>
                        <img id="image" class="img-fluid" src="<?= $upload.$article['medias'][0];?>" alt="" style="margin: 2px;">
                        <?php endif; ?>
                        <h4> <?= $article['title']; ?></h4>
                        <div class="user_details">
                        </div>
                        <p><?= $article['description']; ?></p>
                    </div>
                    <div id="disqus_thread"></div>
                    <script>

                        /**
                         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

                        //var disqus_config = function () {
                        //this.page.url = window.location.href;  // Replace PAGE_URL with your page's canonical URL variable
                        // this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        //};
                        (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://proweb-1.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                </div>

                <!-- Start Blog Post Siddebar -->
                <div class=" sidebar-widgets">
                    <div class="widget-wrap" >
                        <ul id="images">
                            <?php if (isset($article['medias'])):?>
                                <?php foreach ($article['medias'] as $k => $media):?>
                                    <?php if ($k != 0):?>
                                       <li> <img id="image" class="img-fluid" src="<?= $upload.$media;?>" alt="" style="margin: 2px;"></li>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Blog Post Siddebar -->
    </div>
</section>

<?php include 'application/views/_templates/_footer.php'?>
<script src="../../../public/vendors/jquery/jquery-3.2.1.min.js"></script>
<script src="../../../public/vendors/bootstrap/bootstrap.bundle.min.js"></script>
<script src="../../../public/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="../../../public/js/jquery.ajaxchimp.min.js"></script>
<script src="../../../public/js/mail-script.js"></script>
<script src="../../../public/js/main.js"></script>

<script src="../../../public/js/viewer.js" ></script>
<script>

    // View a list of images
    const gallery = new Viewer(document.getElementById('images'));
</script>

</body>
</html>