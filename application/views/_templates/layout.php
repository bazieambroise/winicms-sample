<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title> <?=$all_data['company_name'] ?? 'WINI-SITE';?></title>
  <link rel="icon" href="https://winicms.com/uploads/<?=$logo;?>" type="image/png">

<?php
require 'public/css/style.php';
require 'public/css/slide.php';
require 'public/css/ws-form.php';
require 'public/css/wini_css.php';
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


</head>
<body style="overflow-x: hidden;">

  <!--================Header Menu Area =================-->
  <?php include 'application/views/_templates/_header.php'?>
  <!--================Header Menu Area =================-->

  <main class="site-main">
    <!--================Hero Banner start =================-->


      <!--================Hero Banner end =================-->
    <!--================ Blog slider start =================-->

      <?php foreach ($pages['sections'] as $section): ?>
          <div class="section <?=$section['settings']['classes'] ?? '';?>" style="background-color: <?=$section['settings']['background_color']?>; min-height: 100px;">
              <?php foreach ($section['blocks'] as $block): ?>
                  <div class="block<?php if ($section['settings']['grid_template_columns'] === '1fr 1fr 1fr 1fr'): ?> article <?php endif;?>">
                      <?php foreach ($block['data'] as $data): ?>
                          <!-- JE VERIFIE SI LE TYPE EST UN CAROUSEL -->
                          <?php if ($data['type'] === 'carousel'): ?>
                          <style>
                              <?=$data['content']['settings']['css_styles'];?>
                          </style>
                          <script>
                              <?=$data['content']['settings']['js_code'];?>
                          </script>
                       <section class="<?=$data['content']['settings']['widget_class'];?>" id="<?=$data['content']['settings']['widget_id'];?>">
                                    <div id="slider-animation container" class="carousel slide" data-ride="carousel">
                                          <ul class="carousel-indicators">
                                              <?php foreach ($data['content']['items'] as $key => $slide): ?>
                                                <li data-target="#slider-animation" data-slide-to="<?=$key - 1;?>" class="<?=($key - 1 == 0 ? 'active' : '');?>"></li>
                                              <?php endforeach;?>
                                          </ul>
                                          <div class="carousel-inner">
                                          <?php foreach ($data['content']['items'] as $key => $slide): ?>
                                              <div class="carousel-item <?=($key - 1 == 0 ? 'active' : '');?>">
                                                  <img class="img-fluid" src="<?=$slide['background_image']?>" alt="Wini Cms" style="width: 100%;">
                                                  <div class="text-box">
                                                      <h1 class="wow slideInRight" data-wow-duration="2s"><?=$slide['title'];?></h1>
                                                      <p class="wow slideInLeft" data-wow-duratlion="2s"><?=$slide['subtitle'];?></p>
                                                      <?php if (isset($slide['btn_action']['label']) && ($slide['btn_action']['label']) != null): ?>
                                                        <a href="<?=$slide['btn_action']['external_link']?>" data-target="<?=isset($slide['btn_action']['target']) ? $slide['btn_action']['target'] : '';?>"   class="btn btn-info"> <?=$slide['btn_action']['label']?></a>
                                                      <?php endif;?>
                                                  </div>
                                              </div>
                                          <?php endforeach;?>
                                          </div>
                                          <a class="carousel-control-prev" href="#slider" data-slide="prev">
                                              <span class="carousel-control-prev-icon"></span>
                                          </a>
                                          <a class="carousel-control-next" href="#slider" data-slide="next">
                                              <span class="carousel-control-next-icon"></span>
                                          </a>
                                    </div>
                       </section>
                          <?php endif;?>
                          <!-- //JE VERIFIE SI LE TYPE EST UN CAROUSEL -->

                          <!-- JE VERIFIE SI LE TYPE EST UN COMPOSANT -->
                          <?php if ($data['type'] === 'custom'): ?>
                              <div style="" class="text-center">
                                  <?=$data['content']['html'];?>
                                  <style>
                                      <?=$data['content']['css']?>
                                  </style>
                                  <script>
                                      <?=$data['content']['js']?>
                                  </script>
                              </div>
                          <?php endif;?>
                          <!-- //JE VERIFIE SI LE TYPE EST UN COMPOSANT -->

                          <!-- JE VERIFIE SI LE TYPE EST UN FORMULAIRE -->
                          <?php if ($data['type'] === 'form'): ?>

                                  <?=$data['content'];?>

                          <?php endif;?>
                          <!-- //JE VERIFIE SI LE TYPE EST UN FORMULAIRE -->

                          <!-- JE VERIFIE SI LE TYPE EST UN ARTICLE -->
                          <?php if ($data['type'] === 'article'): ?>
                              <?php if ($section['settings']['grid_template_columns'] === '3fr 1fr'): ?>
                                  <div class="single-recent-blog-post" style="padding: 5px;">
                                      <div class="thumb">
                                          <?php if (isset($data['medias']) && $data['medias'] != null): ?>
                                              <?php foreach ($data['medias'] as $media): ?>
                                                  <center><img class="img-fluid" src="https://winicms.com/uploads/<?=$media;?>" alt=""></center>
                                              <?php endforeach;?>
                                          <?php endif;?>
<!--                                          <ul class="thumb-info">-->
<!--                                              <li><a href="#"><i class="ti-notepad"></i>January 12,2019</a></li>-->
<!--                                          </ul>-->
                                      </div>
                                      <div class="details mt-20">
                                          <a href="blog-single.html">
                                              <h3><?=$data['title']?></h3>

                                          </a>
                                          <?php if (isset($data['setting']['character']) && !empty($data['setting']['character'])): ?>
                                              <p><?=substr($data['description'], 0, ($data['setting']['character']) ?? 200);?></p>
                                          <?php else: ?>
                                              <p><?=$data['description'];?></p>
                                          <?php endif;?>
                                          <?php if (strlen($data['description']) > ($data['setting']['character'] ?? 0) && !empty($data['setting']['btn']['label'])): ?>
                                              <a href="<?='/' . $api_key . '/' . $pages['name'] . '/' . $data['id']?>" class="wc-form-submit-btn"><?=isset($data['setting']['btn']['label']) ? $data['setting']['btn']['label'] : ' ';?></a>
                                          <?php endif;?>
                                      </div>
                                  </div>
                              <?php endif;?>
                              <?php if ($section['settings']['grid_template_columns'] === '1fr 3fr'): ?>
                                  <div class="single-recent-blog-post" >
                                      <div class="thumb">
                                          <?php if (isset($data['medias']) && $data['medias'] != null): ?>
                                              <?php foreach ($data['medias'] as $media): ?>
                                                  <center><img class="img-fluid" src="https://winicms.com/uploads/<?=$media;?>" alt=""></center>
                                              <?php endforeach;?>
                                          <?php endif;?>
<!--                                          <ul class="thumb-info">-->
<!--                                              <li><a href="#"><i class="ti-notepad"></i>January 12,2019</a></li>-->
<!--                                          </ul>-->
                                      </div>
                                      <div class="details mt-20">
                                          <a href="blog-single.html">
                                              <h3><?=$data['title']?></h3>
                                          </a>
                                          <?php if (isset($data['setting']['character']) && !empty($data['setting']['character'])): ?>
                                              <p><?=substr($data['description'], 0, ($data['setting']['character']) ?? 200);?></p>
                                          <?php else: ?>
                                              <p><?=$data['description'];?></p>
                                          <?php endif;?>
                                          <?php if (strlen($data['description']) > ($data['setting']['character'] ?? 0)): ?>
                                              <a href="<?='/' . $api_key . '/' . $pages['name'] . '/' . $data['id']?>" class="wc-form-submit-btn"><?=isset($data['setting']['btn']['label']) ? $data['setting']['btn']['label'] : ' ';?></a>
                                          <?php endif;?>
                                      </div>
                                  </div>
                              <?php endif;?>
                              <?php if ($section['settings']['grid_template_columns'] === '1fr 1fr 1fr 1fr' || $section['settings']['grid_template_columns'] === '1fr 1fr'): ?>
                                  <div>
                                      <center><h3><?=$data['title']?></h3></center>
                                      <?php if (isset($data['medias']) && $data['medias'] != null): ?>
                                              <center><img id="image" class="img-fluid" src="https://winicms.com/uploads/<?=$data['medias'][0];?>" alt=""></center>
                                      <?php endif;?>
                                      <?php if (isset($data['setting']['character']) && !empty($data['setting']['character'])): ?>
                                          <p><?=substr($data['description'], 0, ($data['setting']['character']) ?? 200);?></p>
                                            <?php else: ?>
                                                <p><?=$data['description'];?></p>
                                      <?php endif;?>
                                      <?php if (strlen($data['description']) > ($data['setting']['character'] ?? 0) && !empty($data['setting']['btn']['label'])): ?>
                                          <a href="<?='/' . $api_key . '/' . $pages['name'] . '/' . $data['id']?>" class="wc-form-submit-btn"><?=isset($data['setting']['btn']['label']) ? $data['setting']['btn']['label'] : ' ';?></a>
                                      <?php endif;?>
                                  </div>
                              <?php endif;?>
                          <?php if ($section['settings']['grid_template_columns'] === '4fr'): ?>
                          <div class="single-recent-blog-post" >
                              <div class="thumb">
                                  <?php if (isset($data['medias']) && $data['medias'] != null): ?>
                                      <?php foreach ($data['medias'] as $media): ?>
                                          <center><img class="img-fluid" src="https://winicms.com/uploads/<?=$media;?>" alt=""></center>
                                      <?php endforeach;?>
                                  <?php endif;?>
                                  <!--                                          <ul class="thumb-info">-->
                                  <!--                                              <li><a href="#"><i class="ti-notepad"></i>January 12,2019</a></li>-->
                                  <!--                                          </ul>-->
                              </div>
                              <div class="details mt-20">
                                  <a href="blog-single.html">
                                      <h3><?=$data['title']?></h3>
                                  </a>
                                  <?php if (isset($data['setting']['character']) && !empty($data['setting']['character'])): ?>
                                      <p><?=substr($data['description'], 0, ($data['setting']['character']) ?? 200);?></p>
                                  <?php else: ?>
                                      <p><?=$data['description'];?></p>
                                  <?php endif;?>
                                  <?php if (strlen($data['description']) > ($data['setting']['character'] ?? 0)): ?>
                                      <a href="<?='/' . $api_key . '/' . $pages['name'] . '/' . $data['id']?>" class="wc-form-submit-btn"><?=isset($data['setting']['btn']['label']) ? $data['setting']['btn']['label'] : ' ';?></a>
                                  <?php endif;?>
                              </div>
                          </div>
                      <?php endif;?>
                          <?php endif?>
                          <!-- //JE VERIFIE SI LE TYPE EST UN ARTICLE -->
                      <?php endforeach;?>
                  </div>
              <?php endforeach;?>
          </div>
      <?php endforeach;?>
    <!--================ Blog slider end =================-->
  </main>
  <?php include 'application/views/_templates/_footer.php'?>
  <script src="../../../public/vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="../../../public/vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="../../../public/vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="../../../public/js/jquery.ajaxchimp.min.js"></script>
  <script src="../../../public/js/mail-script.js"></script>
  <script src="../../../public/js/main.js"></script>
<!--  <script src="../../../public/js/slide.js"></script>-->
<!--  <script src="https://winicms.com/public/js/api/v2.0.0/ajax-form-submission.js"></script>-->


</body>
</html>