<?php

/**
 *
 * Configuration for: WINICMS API
 *
 * Here you define global variable and use inside the all site
 *
 */
define('API_KEY', 'uxRHrKi-onFNujTDFMOWQnJykSCVZKcinbQyGvoEVyB');

function getBase()
{
    $base_url = explode('?', $_SERVER['REQUEST_URI']);

    return $base_url[0];
}

function url($key = null, $page = null, $article = null)
{
    $response = getBase() . "?api_key=$key";

    if ($page != null) {
        $response .= "&page_name=$page";

        if ($article != null) {
            $response .= "&article=$article";
        }
    }
    return $response;
}
